FROM alpine/git

 LABEL autor="Denison Ferreira"
 LABEL e-mail="denison.ferreira@hotmail.com.br"

 WORKDIR /home/task

 RUN git clone --branch dev https://gitlab.com/denison.ferreira/my-task.git .
#  COPY . .

 RUN apk update \
    && apk add python3 \
    && apk add py3-pip \
    && apk add --update tzdata \
    && pip install -r requirements.txt \
    && rm -rf /var/cache/apk/*

 ENV TZ=America/Sao_Paulo

 RUN chmod +x task.sh
 RUN chmod +x app.py
#  RUN ./task.sh
  ENTRYPOINT FLASK_APP=/home/task/app.py flask run --host=0.0.0.0
