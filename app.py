from email.policy import default
from multiprocessing import connection
from flask import Flask, request, jsonify, json
from datetime import datetime
import requests
import mysql.connector as mysql
# from mysql.connector import Error
import docker

data_atual = datetime.today()

app = Flask(__name__)

tasks = [

]

def _find_next_id():
    try:
        result = max(task["id"] for task in tasks)
    except ValueError:
        result = 0
    return result+1

# def conectar():
#             db = mysql.connect(
#             host="db",
#             port=3306,
#             user="root",
#             password="secret",
#             database="tasks")

@app.get("/api/job")
def get_tasks():
    try:
        db = mysql.connect(
            host="db",
            port=3306,
            user="root",
            password="secret",
            database="tasks")

        # conectar()
        cursor = db.cursor()
        cursor.execute("SELECT id,nomeTarefa,url FROM tasks.log")
        
        row_headers=[x[0] for x in cursor.description]
        lista = cursor.fetchall()
        json_data=[]
        for result in lista:
            json_data.append(dict(zip(row_headers,result)))

    except mysql.Error as error:
        print("Ocorreu um erro.")
        return ("A lista está vazia.")
    finally:
        if db:
            db.close()

    return str(json_data)

@app.route('/api/job/<idexe>', methods=['POST'])
def search(idexe):
    try:
        db = mysql.connect(
            host="db",
            port=3306,
            user="root",
            password="secret",
            database="tasks")

        cursor = db.cursor()

        cursor.execute("SELECT id,nomeTarefa,url FROM tasks.log WHERE id="+idexe)
        row_headers=[x[0] for x in cursor.description]
        lista = cursor.fetchall()
        json_data=[]
        for result in lista:
            json_data.append(dict(zip(row_headers,result)))

    except mysql.Error as error:
        print("Ocorreu um erro.")
        return ("A lista está vazia.")
    finally:
        if db:
            db.close()

    return str(json_data)

@app.post("/api/job")
def add_tasks():
    if request.is_json:
        task = request.get_json()
        task["id"] = _find_next_id()
        tasks.append(task)
        client = docker.DockerClient(base_url="unix://var/run/docker.sock")
        client = docker.from_env()
        try:
            execution = client.containers.run('denisonferreira/scripttask:latest', 'sh task.sh ' + str(task["id"]))
            status=200
        except:
            status=503

        db = mysql.connect(
            host="db",
            port=3306,
            user="root",
            password="secret",
            database="tasks")

        cursor = db.cursor()

        cursor.execute("INSERT INTO `tasks`.`log` (id, horario, saidascript, statuscode, nomeTarefa, url) VALUES (%s, %s, %s, %s, %s, %s);", (task["id"],data_atual, execution, status,"my-task","http://localhost:9090/eu/my-task.git"))
        db.commit()
        return (execution),200
    else:
        return {"error": "Request must be JSON"}, 415

if __name__ == "__main__":
    app.run(debug=True, port=5000, host="0.0.0.0")

@app.route('/api/job/<idexe>', methods=['DELETE'])
def deletar(idexe):

    try:
        db = mysql.connect(
            host="db",
            port=3306,
            user="root",
            password="secret",
            database="tasks")

        cursor = db.cursor()
        cursor.execute("DELETE FROM log WHERE id="+idexe)
        db.commit()
        result = "Tarefa excluída";

    except mysql.Error as error:
        print("Ocorreu um erro.")
        return ("A lista está vazia.")
    finally:
        if db:
            db.close()
    return str(result)
    