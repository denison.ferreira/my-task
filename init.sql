CREATE DATABASE IF NOT EXISTS tasks;
USE tasks;
CREATE TABLE if not exists `tasks`.`log` (`id` INT NOT NULL AUTO_INCREMENT,`horario` VARCHAR(45) NULL,`saidascript` VARCHAR(45) NULL,`statuscode` VARCHAR(45) NULL,`nomeTarefa` VARCHAR(45) NULL,`url` VARCHAR(45) NULL,PRIMARY KEY (`id`));